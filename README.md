# EthGiving

EthGiving is blockchain powered charity fundraising platform. You can use EthGiving to raise funds for charity directly on the Ethereum blockchain.

> EthGiving is in the very early stages of development, if you would like to help contribute please contact us on https://gitter.im/EthGiving or open an issue on here

## Why the blockchain?

Current platforms, such as justgiving.com, charge a significant fee to charities and fundraisers (typically a monthly subscription for the charity and a 5% fee on all funds raised). We believe we can signficantly reduce or remove this fee by shifting the expensive business & financial logic of maintaining, tracking and releasing funds onto the blockchain.

By using the EthGiving platform, fundraisers and charities will cut out payment processors and will be able to keep more of their raised funds.

## How you can help

We need support from:
- Front end developers (react)
- Back end developers (node)
- Contract developers (solidity)
- Designers
- Marketing and copywriters
- Product Managers & Business Analysts
- Technical documentation writers
- Pilot charities and fundraisers

Check out https://gitlab.com/EthGiving/EthGiving/issues to get started

## Getting started with development

EthGiving uses http://truffleframework.com/, so if you intend to contribute to contract or blockhain related code sure you have that installed first, then:

1. Clone this repo
2. `truffle develop`
3. `npm run start`

Install MetaMask if you intend to do anything that interacts with the blockchain

Then take a look at our issues (https://gitlab.com/EthGiving/EthGiving/issues) to see what we're working on and where you can help.

## Initial Architecture Assumptions

- React application that interacts with the blockchain through web3
- Back-end is a simple Node.js HTTP API 
- Users are expected to be using MetaMask, Mist or another browser supported by web3 for identity & donating funds
- Basic details of fundraisers / charity projects are stored directly on the blockchain (target, funds raised, date of fund release, address to send funds to upon reaching target etc.)
- Smart contracts handle storing and releasing funds once targets are met
- A credit card / alternative payment gateway will be added to allow donations to be collected from donators that are not yet up and running with cryptocurrencies. This will incur a fee which will be transparently shown to the donator against the fee for using Ethereum to donate

## FAQs

### Why no whitepaper?
This is a product, not an pseudo-academic idea.

### Why no ICO?
We don't need a token to do this, donations will be directly in Ethereum (or other currencies), and this is not a hugely compelex dApp that requires a massive amount of funding. If you'd like to donate to help us out or show appreciation you can send us funds on ethereum here:

`0x78739dc861D19A3ecDb7e875Eb8F75883890Ea5C`

### How does EthGiving cover its operating costs?
TBD, some options:

- A small fee levied on each donation
- We run a monthly donation drive on our own platform
- A flat fee per fundraiser