const initialState = {
  data: null
}

const fundraiserReducer = (state = initialState, action) => {
  if (action.type === 'FUNDRAISER_CREATED')
  {
    return Object.assign({}, state, {
      data: action.payload
    })
  }
  return state
}

export default fundraiserReducer
