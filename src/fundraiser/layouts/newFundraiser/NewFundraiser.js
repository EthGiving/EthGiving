import React, { Component } from 'react'
import FundraiserFormContainer from '../../ui/fundraiserform/FundraiserFormContainer'

class NewFundraiser extends Component {
  render() {
    return(
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Create a new fundraiser</h1>
            <p>Enter details here</p>
            <FundraiserFormContainer />
          </div>
        </div>
      </main>
    )
  }
}

export default NewFundraiser
