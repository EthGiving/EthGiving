import React, { Component } from 'react'

class FundraiserForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      title: this.props.title
    }
  }

  onInputChange(event) {
    this.setState({ title: event.target.value })
  }

  handleSubmit(event) {
    event.preventDefault()

    if (this.state.title.length < 2)
    {
      return alert('Please fill in the title.')
    }

    this.props.onProfileFormSubmit(this.state.title)
  }

  render() {
    return(
      <form className="pure-form pure-form-stacked" onSubmit={this.handleSubmit.bind(this)}>
        <fieldset>
          <label htmlFor="title">Fundraiser Title</label>
          <input id="title" type="text" value={this.state.title} onChange={this.onInputChange.bind(this)} placeholder="Title" />
          <span className="pure-form-message">This is a required field.</span>

          <br />

          <button type="submit" className="pure-button pure-button-primary">Next</button>
        </fieldset>
      </form>
    )
  }
}

export default FundraiserForm;
