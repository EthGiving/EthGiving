import { connect } from 'react-redux'
import FundraiserForm from './FundraiserForm'
import { createfundraiser } from './FundraiserFormActions'

const mapStateToProps = (state, ownProps) => {
  return {
    name: state.user.data.name
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onProfileFormSubmit: (name) => {
      event.preventDefault();

      dispatch(createfundraiser(name))
    }
  }
}

const FundraiserFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FundraiserForm)

export default FundraiserFormContainer;
