import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import userReducer from './user/userReducer'
import fundraiserReducer from './fundraiser/fundraiserReducer'
import web3Reducer from './util/web3/web3Reducer'

const reducer = combineReducers({
  routing: routerReducer,
  user: userReducer,
  fundraiser: fundraiserReducer,
  web3: web3Reducer
})

export default reducer
